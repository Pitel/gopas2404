package cz.gopas.kalklacka.calc

import android.app.Application
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import cz.gopas.kalklacka.R
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

class CalcViewModel(app: Application) : AndroidViewModel(app) {
    private val _result = MutableStateFlow<Float?>(null)
    val result: StateFlow<Float?> = _result

    private val prefs = PreferenceManager.getDefaultSharedPreferences(app)

    var ans: Float
        get() = prefs.getFloat(ANS_KEY, Float.NaN)
        private set(value) = prefs.edit {
            putFloat(ANS_KEY, value)
        }

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        viewModelScope.launch {
            delay(1.seconds) // Hardcore math!
            _result.value = when (op) {
                R.id.add -> a + b
                R.id.sub -> a - b
                R.id.mul -> a * b
                R.id.div -> a / b
                else -> Float.NaN
            }.also { ans = it }
        }
    }

    private companion object {
        const val ANS_KEY = "ans"
    }
}
