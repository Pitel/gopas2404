package cz.gopas.kalklacka.calc

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import cz.gopas.kalklacka.R

class ZeroDialog : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?) = MaterialAlertDialogBuilder(requireContext())
        .setMessage(R.string.zero)
        .create()
}
