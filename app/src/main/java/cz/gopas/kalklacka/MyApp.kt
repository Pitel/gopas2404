package cz.gopas.kalklacka

import android.app.Application
import android.provider.Settings.Global
import android.util.Log
import androidx.fragment.app.FragmentManager
import com.google.android.material.color.DynamicColors
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.time.Duration.Companion.seconds

class MyApp : Application() {
    val logs = mutableListOf<String>()
    private var exclusive = 0
    private val mutex = Mutex()

    init {
        FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
        Log.d(TAG, "Init 1")
        val async = GlobalScope.async {
            Log.d(TAG, "Async on ${Thread.currentThread().name}")
            throw Exception("Boom!")
        }
        GlobalScope.launch {
            Log.d(TAG, "Launch on ${Thread.currentThread().name}")
            delay(1.seconds)
            try {
                async.await()
            } catch (e: Exception) {
                Log.w(TAG, e)
            }
        }
        GlobalScope.launch { useExclusive { exclusive = 10 } }
        GlobalScope.launch { useExclusive { exclusive = 20 } }
        GlobalScope.launch { useExclusive { Log.d(TAG, "$it") } }
        Log.d(TAG, "Init 2")
    }

    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)
    }

    suspend fun useExclusive(block: (Int) -> Unit) = mutex.withLock {
        block(exclusive)
    }

    private companion object {
        private val TAG = MyApp::class.simpleName
    }
}
