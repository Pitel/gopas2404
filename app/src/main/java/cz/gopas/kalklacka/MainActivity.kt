package cz.gopas.kalklacka

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import cz.gopas.kalklacka.about.AboutFragment
import cz.gopas.kalklacka.calc.CalcFragment
import cz.gopas.kalklacka.databinding.ActivityMainBinding
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {
//    private val aText: TextInputEditText by lazy { findViewById(R.id.a_text) }
//    private lateinit var bText: TextInputEditText
//    private lateinit var ops: RadioGroup
//    private lateinit var res: MaterialTextView
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Create")

        thread {
            Log.d(TAG, "Running on ${Thread.currentThread().name}")
            runOnUiThread {
                Log.d(TAG, "Running on ${Thread.currentThread().name}")
            }
        }

        Log.d(TAG, "Intent ${intent.dataString}")
//        supportFragmentManager.commit {
//            replace(R.id.container, CalcFragment())
//            addToBackStack(null)
//        }
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Resume")
        log("Resume")
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Pause")
        log("Pause")
    }

    private fun log(msg: String) {
        (application as MyApp).logs += msg
        Log.d(TAG, "${(application as MyApp).logs}")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.about -> {
            if (binding.container.getFragment<Fragment>() !is AboutFragment) {
                supportFragmentManager.commit {
                    replace(R.id.container, AboutFragment("Honza Kaláb"))
                    addToBackStack(null)
                }
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putCharSequence(RES_KEY, res.text)
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        res.text = savedInstanceState.getCharSequence(RES_KEY)
//    }

    private companion object {
        private val TAG = MainActivity::class.simpleName
        private const val RES_KEY = "res"
    }
}
