package cz.gopas.kalklacka.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import cz.gopas.kalklacka.R
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class HistoryFragment : Fragment(R.layout.fragment_history) {
    private val vm: HistoryViewModel by viewModels(ownerProducer = { requireActivity() })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rv = view as RecyclerView
        rv.setHasFixedSize(true)
//        lifecycleScope.launch {
//            val data = List(10_000) { HistoryEntity(it.toFloat()) }
//            vm.db.add(*data.toTypedArray())
//        }
        val adapter = HistoryAdapter {
            vm.selected.value = it
            parentFragmentManager.popBackStack()
        }
        rv.adapter = adapter
        vm.db.getAll().onEach {
            adapter.submitList(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }
}
