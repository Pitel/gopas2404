package cz.gopas.kalklacka.calc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import cz.gopas.kalklacka.R
import cz.gopas.kalklacka.databinding.FragmentCalcBinding
import cz.gopas.kalklacka.history.HistoryEntity
import cz.gopas.kalklacka.history.HistoryFragment
import cz.gopas.kalklacka.history.HistoryViewModel
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.text.NumberFormat

class CalcFragment : Fragment() {
    private var binding: FragmentCalcBinding? = null
    private val vm: CalcViewModel by viewModels()
    private val historyVm: HistoryViewModel by viewModels(ownerProducer = { requireActivity() })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCalcBinding.inflate(inflater, container, false)
        .also { binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.run {
            share.setOnClickListener { share() }
            calc.setOnClickListener { calc() }
            ans.setOnClickListener { bText.setText("${vm.ans}") }
            history.setOnClickListener {
                parentFragmentManager.commit {
                    replace(R.id.container, HistoryFragment())
                    addToBackStack(null)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        vm.result.filterNotNull().onEach {
            binding?.res?.text = "$it"
            historyVm.db.add(HistoryEntity(it))
        }.launchIn(viewLifecycleOwner.lifecycleScope)
        historyVm.selected.filterNotNull().onEach {
            binding?.aText?.setText("$it")
            historyVm.selected.value = null
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun calc() {
        Log.d(TAG, "Calc!")

        val nf = NumberFormat.getInstance()
        Log.d(TAG, nf.parse("5,5")?.toFloat().toString())
        Log.d(TAG, nf.format(5.5))

        val a = binding?.aText?.text.toString().toFloatOrNull() ?: Float.NaN
        val b = binding?.bText?.text.toString().toFloatOrNull() ?: Float.NaN
        val op = binding?.ops?.checkedRadioButtonId ?: 0
        if (op == R.id.div && b == 0f) {
            ZeroDialog().show(childFragmentManager, null)
        } else {
            vm.calc(a, b, op)
        }
    }

    private fun share() {
        val intent = Intent(Intent.ACTION_SEND)
            .setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding?.res?.text))
        startActivity(intent)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}
