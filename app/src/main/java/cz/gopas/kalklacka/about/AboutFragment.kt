package cz.gopas.kalklacka.about

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.google.android.material.textview.MaterialTextView
import cz.gopas.kalklacka.R

class AboutFragment() : Fragment(R.layout.fragment_about) {
    constructor(name: String) : this() {
        arguments = bundleOf(NAME_KEY to name)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (view as MaterialTextView).text = getString(
            R.string.about_text,
            requireArguments().getString(NAME_KEY)
        )
    }

    private companion object {
        private const val NAME_KEY = "name"
    }
}
